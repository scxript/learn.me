<?php
// /**
//  * Test Class
//  */
class Test { 	//blueprint, pattern, object factory
	
	//properties = variables	
	protected $ourProperty = "i am here"; 

	//getter
	function getProperty()
	{
		return $this->ourProperty;
	}

	//setter
	function setProperty($value)
	{
		$this->ourProperty = $value;
	}

	//methods = functions	
	function returnString() { //public is default for methods/functions
		echo "from insade of Object " . $this->ourProperty;
	}

	function echoOut(){
		$this->returnString();

	}
}

//instantiate

$object = new Test();

//var_dump($object->returnString()); 
//echo $object->returnString(); 

$object->ourProperty = 'value changed';

// echo $object->ourProperty();

// $object->setProperty('i am the new value');

// echo $object->getProperty();

//stigao sam do 2.2 



