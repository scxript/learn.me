<!--https://www.youtube.com/watch?v=UjCP5S2FNXQ-->

<?php
class BaseClass {
    function __construct() {
        print "In BaseClass constructor<br>";
    }
}

class SubClass extends BaseClass {
    function __construct() {
        parent::__construct();
        print "In SubClass constructor<br>";
    }
}

class OtherSubClass extends SubClass {
    // inherits BaseClass's constructor
}

// In BaseClass constructor
// $obj = new BaseClass();

// In BaseClass constructor
// In SubClass constructor
// $obj = new SubClass();

// In BaseClass constructor
// $obj = new OtherSubClass();


?>

<?php
class MyClass {

    public $var  = 'I like OOP';

    public function __construct(){
        $this->var = 'Meh, OOP is nice';
    }

    public function my_function(){
        echo $this->var;
    }
}

$myClass = new MyClass();
$myClass->my_function();

?>







