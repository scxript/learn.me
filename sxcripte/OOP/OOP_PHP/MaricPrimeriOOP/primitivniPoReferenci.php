<?php
$kafa = [
    "naziv"=>"Moka",
    "cena"=>8
];
//Primitivni tipovi
// $k = $kafa;
// $k['cena'] = 10;
// echo $k['cena'];
//Po referenci
$kafa = (object)$kafa;	//ekplicitnu konverziju u stvar Objekat
// $k = $kafa;
/*
$kafa->cena = 10;
var_dump($kafa);
*/
// $kafa = null;
// print_r($kafa);

echo $kafa->naziv;
echo "<br>";
echo $kafa->cena;

?>
<?php

/**
 * Artikli primer prosto jednostavno
 */
//class Article{
//    public $title;
//    public $content;
//}
//
//$nv = new Article;
//$nv->title = "Pera Lozac cepa atome";
//$nv->content = "Danas je Pera lozac pocepao atom";
//?>
<!---->
<!--<h3>--><?//=$nv->title?><!--</h3>-->
<!--<p>--><?//=$nv->content?><!--</p>-->
?>



