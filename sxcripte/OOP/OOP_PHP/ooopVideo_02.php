<?php

// /**
//  * Test Class
//  */

class Test { 	//blueprint, pattern, object factory
	
	//properties = variables	
	protected $ourProperty = "I am here"; 
	protected $ourProperty2 = "2"; 
	protected $ourProperty3 = "3"; 
	protected $ourProperty4 = "4"; 
	protected $ourProperty5 = "5"; 


	//getter
	function getProperty($name)
	{
		return $this->$name;
	}

	//setter
	function setProperty($value)
	{
		$this->ourProperty2 = $value;
	}

	//methods = functions	
	function returnString() { //public is default for methods/functions
		echo "from insade of Object " . $this->ourProperty;
	}

	function echoOut(){
		$this->returnString();
	}
}



//instantiate

$object = new Test();

// var_dump($object->returnString()); 

//$object->ourProperty = 'value changed';

// echo $object->ourProperty;

// $object->setProperty('i am the new value');

// echo $object->getProperty('ourProperty2');

echo $object->echoOut();








