<!-- 

 A string literal can be specified in four different ways:

    single quoted
    double quoted
    heredoc syntax
    nowdoc syntax (since PHP 5.3.0)

    
 -->



<?php
// echo <<<EOD
// Example of string
// spanning multiple lines
// using heredoc syntax.
// EOD;


// /* More complex example, with variables. */
// class foo
// {
//     var $foo;
//     var $bar;

//     function __construct()
//     {
//         $this->foo = 'Foo';
//         $this->bar = array('Bar1', 'Bar2', 'Bar3');
//     }
// }

// $foo = new foo();
// $name = 'MIo';

// echo <<<EOT
// My name is "$name". I am printing some $foo->foo.
// Now, I am printing some {$foo->bar[2]}.
// This should print a capital 'A': \x41
// EOT;
?>
