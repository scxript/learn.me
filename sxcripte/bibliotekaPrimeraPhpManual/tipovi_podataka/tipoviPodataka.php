<!--
When converting to boolean, the following values are considered FALSE: 
    
    the boolean FALSE itself
    the integers 0 and -0 (zero)
    the floats 0.0 and -0.0 (zero)
    the empty string, and the string "0"
    an array with zero elements
    the special type NULL (including unset variables)
    SimpleXML objects created from empty tags

  -->



<?php
