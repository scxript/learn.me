<?php
/*

https://secure.php.net/manual/en/language.types.type-juggling.php
https://secure.php.net/manual/en/language.types.string.php#language.types.string.conversion

*/	// (int), (integer) - cast to integer
	// (bool), (boolean) - cast to boolean
	// (float), (double), (real) - cast to float
	// (string) - cast to string
	// (array) - cast to array
	// (object) - cast to object
	// (unset) - cast to NULL

$a = null;
echo (bool)$a; // 1234 (not 1235)
echo "<br>";
$a = -1234.56;
echo (int)$a; // -1234



