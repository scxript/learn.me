<!--Ovde je GET Method primer-->
<!--Welcome --><?php //echo $_GET["name"]; ?><!--.<br />-->
<!--You are --><?php //echo $_GET["age"]; ?><!-- years old!-->


<!--Ovde je POST Method primer-->
<!--Welcome --><?php //echo $_POST["name"]; ?><!--.<br />-->
<!--You are --><?php //echo $_POST["age"]; ?><!-- years old!-->



<!--The $_REQUEST Variable
The PHP $_REQUEST variable contains the contents of both $_GET, $_POST, and $_COOKIE.
The PHP $_REQUEST variable can be used to get the result from form data sent with both the GET and POST methods.-->

Welcome <?php echo $_REQUEST["name"]; ?>.<br />
You are <?php echo $_REQUEST["age"]; ?> years old!

