<?php
//global scope
$x = 10;
function var_scope()
{
//local scope
    $y=20;
    echo "The value of x is :  $x "."<br />";
    echo "The value of y is :  $y"."<br />";
}
var_scope();
echo "The value of x is :  $x"."<br />";
echo "The value of y is :  $y ";
?>
