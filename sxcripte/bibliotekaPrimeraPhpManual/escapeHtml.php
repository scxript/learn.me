<!-- Escape html in PHP -->
<!-- Example #1 Advanced escaping using conditions  -->

<?php $expression = false; ?>

<?php if ($expression == true): ?>
  This will show if the expression is true.
<?php else: ?>
  Otherwise this will show.
<?php endif; ?>

<!-- Uprisceno -->
<?php 
// $x = 10; 
// $y = 20; 
// if ($expression == true): 
// 	echo $x;
// else: 
// 	echo $y;
//   endif; 
?>










