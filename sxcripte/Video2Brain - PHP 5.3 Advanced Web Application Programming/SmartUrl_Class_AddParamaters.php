<!--ADD Parameter-s Lesson II-->
<?php
$smartUrl = new SmartUrl('http://www.google.rs');
$smartUrl->addParameter('id',123);
$smartUrl->addParameter('status','print');
$smartUrl->addParameter('information','yes');
echo $smartUrl->render();


class SmartUrl{
    //inernal varibales
	private $m_baseUrl = '';
	private $m_parameters = array();
    /**
     * SmartUrl constructor.
     * @param $baseUrl
     */
    function __construct($baseUrl){
        $this->m_baseUrl = $baseUrl;
	}
    /**add a parameter
     * @param $parameterKey
     * @param $parameterValue
     */
    public function addParameter($parameterKey, $parameterValue){
        $this->m_parameters[$parameterKey] = $parameterValue;
    }
    /** render
     * @return string
     */
    public function render()
	{
        $r = '';

        //variables
        $baseUrl = $this->m_baseUrl;
        $parameters = $this->m_parameters;
		//built it
        /** @var TYPE_NAME $baseUrl */
        $r .= $baseUrl;
        if (count($parameters) > 0){
            $r .= '?';
            $index = 0;
            foreach ($parameters as $parameterKey => $parameterValue){

                //add & if not first time
                if ($index>=1){
                    $r .= '&';
                }
                //build
                $r .= $parameterKey . '=' . $parameterValue;

                //increment
                $index++;
            }
        }
        return $r;
	}
}