<!--Lesson I-->
<?php

$smartUrl = new SmartUrl('http://www.google.rs');
echo $smartUrl->render();
class SmartUrl{
    //inernal varibales
	private $m_baseUrl;
    /**
     * SmartUrl constructor.
     * @param $baseUrl
     */
    function __construct($baseUrl)
	{
        $this->m_baseUrl = $baseUrl;
	}
    /**render
     *@return string
     */
    public function render()
	{
        $r = '';

        //variabales
        $baseUrl = $this->m_baseUrl;
		//built it
        /** @var TYPE_NAME $baseUrl */
        $r .= $baseUrl;
        return $r;
	}
}