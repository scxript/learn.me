<!--ADD Property-s Lesson III-->
<!--FIXME: !!Nemam '/ '??? -->
<?php
//todo:Proveriti Kod nesto nije u redu
$smartUrl = new SmartUrl('http://www.google.rs');
$smartUrl->setFile('codeExample.php');
$smartUrl->addParameter('id',123);
$smartUrl->addParameter('status','print');
$smartUrl->addParameter('information','yes');
echo $smartUrl->render();


class SmartUrl{
    /**
     * @param string $m_file
     */
    private $m_file = '';
    public function setFile($value){
        $this->m_file = $value;
    }
    public function getFile(){
        return $this->m_file;
    }

    //inernal varibales
    private $m_baseUrl = '';
    private $m_parameters = array();
    /**
     * SmartUrl constructor.
     * @param $baseUrl
     */
    function __construct($baseUrl){
        $this->m_baseUrl = $baseUrl;
    }
    /**add a parameter
     * @param $parameterKey
     * @param $parameterValue
     */
    public function addParameter($parameterKey, $parameterValue){
        $this->m_parameters[$parameterKey] = $parameterValue;
    }
    /** render
     * @return string
     */
    public function render()
    {
        $r = '';

        //variables
        $baseUrl = $this->m_baseUrl;
        $parameters = $this->m_parameters;
        $file = $this->m_file;

        /** build it */
        if (trim($file) != '') {
            $r .= $baseUrl . '/' . $file;
        }else{
            $r .= $baseUrl;
        }
        /** add The $parameters */
        if (count($parameters) > 0){
            $r .= '?';
            $index = 0;
            foreach ($parameters as $parameterKey => $parameterValue){

                //add & if not first time
                if ($index>=1){
                    $r .= '&';
                }
                //build
                $r .= $parameterKey . '=' . $parameterValue;

                //increment
                $index++;
            }
        }
        return $r;
    }
}